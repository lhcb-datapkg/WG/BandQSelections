################################################################################
## DaVinci configuration
from Configurables import DaVinci
#from PhysConf.MicroDST import uDstConf
rootInTes = "/Event/BandQ"
#uDstConf ( rootInTes )
DaVinci().Input = ["Sel.BandQ.mdst"]
DaVinci().TupleFile = "output.root"

DaVinci().RootInTES = rootInTes

DaVinci().InputType = "DST" 
DaVinci().DataType = "2016"
DaVinci().Simulation = False
DaVinci().Lumi = True


################################################################################
## nTuple configuration
import os
from PhysConf.Selections import RebuildSelection, AutomaticData
input_line = AutomaticData("/Event/BandQ/Phys/%s/Particles"%(os.env['TESTLINE']))  

from DecayTreeTuple.Configuration import *
t = DecayTreeTuple ("TestTree")
t . Decay = "X"
t . Branches = { 'X':'X' }
t . Inputs = [input_line.outputLocation()]
tistos = t.addTupleTool("TupleToolTISTOS")
tistos.Verbose = True
tistos.TriggerList = ["L0MuonDecision", "L0DiMuonDecision", "Hlt1TrackMVADecision", "Hlt2DiMuonJpsiDecision"]

t.ToolList += [ "TupleToolRecoStats" ]

DaVinci().UserAlgorithms = [input_line, t]

################################################################################
## Debugging stuff
# TES explorer
from Configurables import StoreExplorerAlg
DaVinci().appendToMainSequence( [ StoreExplorerAlg(PrintEvt=100) ] ) 




from Configurables import DataOnDemandSvc
dod = DataOnDemandSvc()


## 1. Copy/Link ODIN, RawEvent and RecSummary
#
from Configurables import Gaudi__DataLink as Link


for link in [
  Link ( "LinkHlt1SelReports", 
          Target = '/Event/BandQ/Hlt1/SelReports',
          What   = '/Event/Hlt1/SelReports'
        ),
  ]:

    dod.AlgMap [ link.Target ] = link

