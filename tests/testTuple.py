import ROOT

#===============================================================================
# Steering
#===============================================================================
fname = {
#  "Stripping":  "output-stripping.root" ,
#  "Turbo":      "output-turbo.root" ,
  "TestTree" : "output.root",
}

treeName      = "TestTree/DecayTree"

variablesToShow = [
  "X_M",
  "X_IPCHI2_OWNPV",
  "X_FD_OWNPV",
  "X_L0DiMuonDecision_TOS",
  "X_Hlt1TrackMVADecision_TOS",
  "nTracks",
  "nCandidate",
  "totCandidates",
]


#===============================================================================



class logger:
  def __init__ ( self, key ):
    self.buffer = []
    self.key = key
    self.status = True

  def __call__(self, title, check):
    chain = self.key
    self.buffer.append ("{:>10} {:<20} | {:} | {:}".format ( chain, title, str(self.status), bool(check()) ))

  def __del__ ( self ):
    print 
    for line in self.buffer: print line

log = {k:logger(k) for k in fname.keys()}



for key, filename in [(k,fname[k]) for k in fname.keys()]:
  f = ROOT.TFile.Open ( filename )
  log[key] ( 'File exists', lambda : f)

  try: 
    t = f.Get (treeName)
    log[key] ( 'Load tree', lambda: True )
  except:
    log[key] ( 'Load tree', lambda: False)

  try: 
    print "Entries of", key, t.GetEntries()
    log[key] ( 'Tree was filled', lambda: t.GetEntries() > 0 )
  except:
    log[key] ( 'Tree was filled', lambda: False )

  try:
    t.Scan ( ":".join(variablesToShow), "", "", 15 )
  except:
    log[key] ( "Accessing branches", lambda: False )

  
  try:
    log[key] ( "TISTOS of L0", lambda: t.GetMaximum ( "X_L0DiMuonDecision_TOS" ) > 0 )
  except:
    log[key] ( "TISTOS of L0", lambda: False )

  try:
    log[key] ( "TISTOS of Hlt1", lambda: t.GetMaximum ( "X_Hlt1Global_TOS" ) > 0 )
  except:
    log[key] ( "TISTOS of Hlt1", lambda: False )


  t.Draw ( "totCandidates>>h", "nCandidate == 0", 'goff' )
  print "Multiplicity. Mean: {:2.2} | Max: {:d}" . format (
     float(t.GetHistogram().GetMean()),
     int (t.GetMaximum ( 'totCandidates' ))
    )

del log













