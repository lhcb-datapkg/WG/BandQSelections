################################################################################
## DaVinci configuration
from Configurables import DaVinci
rootInTes = "/Event/BandQ/Turbo"
DaVinci().Input = ["Sel.BandQ.mdst"]
DaVinci().TupleFile = "output.root"

from Configurables import DaVinci 

DaVinci().InputType = "DST" 
DaVinci().DataType = "2016"
DaVinci().Simulation = False
DaVinci().Lumi = True


################################################################################
## nTuple configuration
import os
from PhysConf.Selections import RebuildSelection, AutomaticData
input_line = AutomaticData("/Event/BandQ/Turbo/Phys/%s/Particles"%(os.environ['TESTLINE']))  

from DecayTreeTuple.Configuration import *
t = DecayTreeTuple ("TestTree")
t . Decay = "X"
t . Branches = { 'X':'X' }
t . Inputs = [input_line.outputLocation()]
tistos = t.addTupleTool("TupleToolTISTOS")
tistos.Verbose = True
tistos.TriggerList = ["L0MuonDecision", "L0DiMuonDecision", "Hlt1TrackMVADecision", "Hlt2DiMuonJpsiDecision"]

t.ToolList += [ "TupleToolRecoStats" ]

DaVinci().UserAlgorithms = [input_line, t]

################################################################################
## Debugging stuff
# TES explorer
from Configurables import StoreExplorerAlg
DaVinci().appendToMainSequence( [ StoreExplorerAlg(PrintEvt=100) ] ) 



