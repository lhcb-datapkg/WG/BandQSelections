#!/bin/bash
# run with lb-run DaVinci/v42r6p1 test_line_turbo.sh 

export PYTHONPATH=./BandQSelections/:$PYTHONPATH
export BANDQCONFIGROOT=../BandQConfig

gaudirun.py \
    $BANDQCONFIGROOT/options/DataType-2016.py          \
    $BANDQCONFIGROOT/options/Turbo2016.py              \
    $BANDQCONFIGROOT/options/EvtMax1000.py             \
    $BANDQCONFIGROOT/options/input-turbo.py            \
    $@

export TESTLINE=`cat list_of_lines.log`

gaudirun.py \
    tests/DaVinci-Turbo.py

python \
    tests/testTuple.py
