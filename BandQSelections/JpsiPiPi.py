#!/usr/bin/env gaudirun.py
# -*- coding: utf-8 -*-
# =============================================================================
## @file WGProdLine.py
#  Base class for WGProduction lines
#  @attention DataType and CondDB are not specified!
#  Usage: override and implement selection()
#  
#  @author Lucio Anderlini lucio.anderlini@cern.ch
#  @date   2017-07-10
# =============================================================================
""" 
Selection for Jpsi + pi + pi 

"""

from WGProdTools import WGProdLine

################################################################################
from PhysConf.Selections import FilterSelection
from PhysSelPython.Wrappers import AutomaticData 
from PhysConf.Selections import RebuildSelection
from PhysConf.Selections import SimpleSelection
from GaudiConfUtils.ConfigurableGenerators import CombineParticles 

class JpsiPiPi ( WGProdLine ):
  defaults = {
    #### Input TES locations ####
    "jpsiTES"  : 'Hlt2DiMuonJPsiTurbo/Particles' ,
    "pionTES"  : 'Phys/StdAllNoPIDsPions/Particles' , 

    #### Name of the preselected Jpsi for possible sharing/clashing with other lines
    "jpsiPreselName" : 'JPSI',

    #### Selection strategy for "JPSI", possibly shared among lines
    "jpsiMassMin" : 2990,
    "jpsiMassMax" : 3210,
    "muonIdCut"   : "ISMUON" ,

    #### Selection strategy of the pions
    "pionKinematicsCut" : "(PT > 450)",
    "pionPIDCut"        : "(PIDK < 5)",

    #### Combination strategy
    "decay"          : " X_1(3872) -> J/psi(1S) pi+ pi- ",
    "motherCut"      : "(VFASPF(VCHI2PDOF) < 9) & (in_range(0, DTF_CHI2NDOF(False), 5)) & in_range(3500, M, 4100*MeV)",
    "combinationCut" : " ( in_range ( 3450, AM, 4150 ) ) & (ADOCACHI2CUT(25, '') )",
  }



  def selection ( self, cfg ):
    # define here the selection strategy

    #### Converts TES locations into AutomaticData
    self.psi = AutomaticData ( cfg["jpsiTES"] ) 
    self.pi  = RebuildSelection ( AutomaticData ( cfg["pionTES"] ) )
    
    #### Filter the Jpsi 
    psi = FilterSelection (
        self.name() + cfg["jpsiPreselName"] + "Sel",
        [ self.psi ] ,
        Code  = """
        in_range ( {jpsiMassMin}, M , {jpsiMassMax} )
        & CHILDCUT ( 1 , {muonIdCut} ) 
        & CHILDCUT ( 2 , {muonIdCut} )
        """.format ( **cfg )
        )


    #### Filter the pions
    pi = FilterSelection (
          self.name() + "Pions",
          [ self.pi ],
          Code = " & ".join ( [ cfg["pionKinematicsCut"], cfg["pionPIDCut"] ] )
        )


    #### Describe the combination
    print self.name()
    sel = SimpleSelection (
        self.name() + "Line",
        CombineParticles   ,
        [ psi, pi ] ,
        DecayDescriptors = [  cfg["decay"]  ] ,
        CombinationCut   = cfg["combinationCut"],
        MotherCut        = cfg["motherCut"]     ,
      )

    return sel
    



################################################################################
## Interface for automatic testing
################################################################################

if __name__ == '__builtin__':
  # if the file is included as an option, it includes the line. 
  # while waits for configuration if it is import-ed 
  JpsiPiPi('WGProdLineJpsiPiPi') 
  
  f = file('list_of_lines.log', 'w')
  # name of the selection returned from selection() function
  f.write ( 'JpsiPiPiLine' )
  f.close()

  

