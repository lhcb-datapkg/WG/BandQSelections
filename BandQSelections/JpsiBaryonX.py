#!/usr/bin/env gaudirun.py
# -*- coding: utf-8 -*-
# =============================================================================
## @file WGProdLine.py
#  Base class for WGProduction lines
#  @attention DataType and CondDB are not specified!
#  Usage: override and implement selection()
#  
#  @author Marian Stahl    marian.stahl@cern.ch
#  @author Lucio Anderlini lucio.anderlini@cern.ch
#  @date   2018-12-13
# =============================================================================
""" 
Selections for Jpsi + Baryon + X

"""

from WGProdTools import WGProdLine

################################################################################
from PhysSelPython.Wrappers import AutomaticData
from Configurables import DaVinci
from DaVinci.Configuration import *
from PhysConf.Filters import LoKi_Filters
from PhysConf.Selections import MergedSelection, SimpleSelection, FilterSelection
from GaudiConfUtils.ConfigurableGenerators import CombineParticles

class JpsiBaryonX ( WGProdLine ):
  defaults = {
    #### Input TES locations ####
    "JpsiTES"         : '/Event/Dimuon/Phys/FullDSTDiMuonJpsi2MuMuDetachedLine/Particles' ,
    "pTES"            : 'Phys/StdAllLooseProtons/Particles' ,
    "KTES"            : 'Phys/StdAllLooseKaons/Particles' ,
    "piTES"           : 'Phys/StdAllLoosePions/Particles' ,
    "piDTES"          : 'Phys/StdLooseANNDownPions/Particles' ,   #ProbNNpi>0.03
    "piUTES"          : 'Phys/StdLooseANNUpPions/Particles' ,     #ProbNNpi>0.03
    "KDTES"           : 'Phys/StdLooseANNDownKaons/Particles' ,   #ProbNNk >0.05
    "pDTES"           : 'Phys/StdLooseANNDownProtons/Particles' , #ProbNNp >0.04
    "pi0MerTES"       : 'Phys/StdLooseMergedPi0/Particles' ,
    "pi0ResTES"       : 'Phys/StdLooseResolvedPi0/Particles' ,
    "pi0DalTES"       : 'Phys/StdLooseDalitzPi0/Particles' ,
    #"KSLLTES"         : 'Phys/StdVeryLooseKsLL/Particles', #can be used in the future
    "KSDDTES"         : 'Phys/StdLooseKsDD/Particles',
    "phiTES"          : 'Phys/StdLoosePhi2KK/Particles' ,
    "LambdaDDTES"     : 'Phys/StdLooseLambdaDD/Particles' ,
    "LambdaLLTES"     : 'Phys/StdVeryLooseLambdaLL/Particles' ,

    #### Names of preselected particles for possible sharing/clashing with other lines
    "JpsiPreselName"  : 'JpsiHD',
    "KSLLPreselName"  : 'KSLLHD',
    "phiPreselName"   : 'phiHD',
    "SigmaPreselName" : 'SigmaHD',
    "XiPreselName"    : 'XiHD',
    "OmegaPreselName" : 'OmegaHD',

    #### Decay descriptors
    # intermediate
    "KSLLdecay"       : "KS0 -> pi+ pi-",
    "Sigmadecay"      : "[Sigma+ -> p+ pi0]cc",
    "Xidecay"         : "[Xi- -> Lambda0 pi-]cc",
    "Omegadecay"      : "[Omega- -> Lambda0 K-]cc",
    # final
    "Jppdecay"        : "B_s0 -> J/psi(1S) p+ p~-",
    "Jppidecay"       : "[Lambda_b0 -> J/psi(1S) p+ pi-]cc",
    "JpKdecay"        : "[Lambda_b0 -> J/psi(1S) p+ K-]cc",
    "JpKSpidecay"     : "[Lambda_b0 -> J/psi(1S) p+ KS0 pi-]cc",
    "Jppipipidecay"   : "[Lambda_b0 -> J/psi(1S) p+ pi- pi+ pi-]cc",
    "JpKpipidecay"    : "[Lambda_b0 -> J/psi(1S) p+ K- pi+ pi-]cc",
    "JpKKKdecay"      : "[Lambda_b0 -> J/psi(1S) p+ K- K+ K-]cc",
    "JpKKdecay"       : "[Xi_b- -> J/psi(1S) p+ K- K-]cc",
    "JpKpidecay"      : "[Xi_b- -> J/psi(1S) p+ K- pi-]cc",
    "JpKSKdecay"      : "[Xi_b0 -> J/psi(1S) p+ KS0 K-]cc",
    "JpKpiKdecay"     : "[Xi_b0 -> J/psi(1S) p+ K- K- pi+]cc",
    "Xibpidecay"      : "[Xi_b*0 -> Xi_b- pi+]cc",
    "Jpppidecay"      : "[B_c- -> J/psi(1S) p+ p~- pi-]cc",
    "JLpdecay"        : "[B- -> J/psi(1S) Lambda0 p~-]cc",
    "JLLdecay"        : "B_s0 -> J/psi(1S) Lambda0 Lambda~0",
    "JLdecay"         : "[Lambda_b0 -> J/psi(1S) Lambda0]cc",
    "JLPiPidecay"     : "[Lambda_b0 -> J/psi(1S) Lambda0 pi+ pi-]cc",
    "JLPhidecay"      : "[Lambda_b0 -> J/psi(1S) Lambda0 phi(1020)]cc",
    "JSPidecay"       : "[Lambda_b0 -> J/psi(1S) Sigma+ pi-]cc",
    "JXiKdecay"       : "[Lambda_b0 -> J/psi(1S) Xi- K+]cc",
    "JLKdecay"        : "[Xi_b- -> J/psi(1S) Lambda0 K-]cc",
    "JLKPidecay"      : "[Xi_b0 -> J/psi(1S) Lambda0 pi+ K-]cc",
    "JLKSdecay"       : "[Xi_b0 -> J/psi(1S) Lambda0 KS0]cc",
    "JXidecay"        : "[Xi_b- -> J/psi(1S) Xi-]cc",
    "JXiPidecay"      : "[Xi_b0 -> J/psi(1S) Xi- pi+]cc",
    "JXiPiPidecay"    : "[Xi_b- -> J/psi(1S) Xi- pi+ pi-]cc",
    "JOmegadecay"     : "[Omega_b- -> J/psi(1S) Omega-]cc",
    "JOmegaPiPidecay" : "[Omega_b- -> J/psi(1S) Omega- pi+ pi-]cc",

    #### Selection strategy for tracks and intermediate particles
    "JpsiMassRange"   : "in_range(3040,M,3150)",
    "hLKinCut"        : "(MIPCHI2DV(PRIMARY)>4.)", #long tracks
    "hDKinCut"        : "(MIPCHI2DV(PRIMARY)>9.)", #downstream tracks
    "pi0pT"           : 700.,
    "pi0CL"           : 0.1,
    "ANNCut"          : 0.05, #used for long pi, K and p

    #### Combination- and mother cuts (Hyperons)
    "KSLLcombCut"     : "(ADAMASS('KS0') < 50.*MeV) & (ADOCACHI2CUT(30, ''))",
    "SigmacombCut"    : "(ADAMASS('Sigma+')<120*MeV) & (AMAXDOCA('')< 2.*mm)",
    "XicombCut"       : "(ADAMASS('Xi-')<60*MeV) & (ADOCACHI2CUT(30,''))",
    "OmegacombCut"    : "(ADAMASS('Omega-')<60*MeV) & (ADOCACHI2CUT(30,''))",
    "KSLLCut"         : "(ADMASS('KS0') < 35.*MeV) & (VFASPF(VCHI2PDOF)<15) & (BPVVDCHI2>4.)",
    "SigmaCut"        : "(ADMASS('Sigma+')<100*MeV ) & (VFASPF(VCHI2PDOF)< 20) & (BPVVDCHI2>40.) & (BPVLTIME()>4.*ps)",
    "XiCut"           : "(ADMASS('Xi-')<40*MeV) & (VFASPF(VCHI2PDOF)<15) & (BPVVDCHI2>50.0) & (D2DVVDCHI2SIGN(1)>30.)",
    "OmegaCut"        : "(ADMASS('Omega-')<40*MeV) & (VFASPF(VCHI2PDOF)<15) & (BPVVDCHI2>40.) & (D2DVVDCHI2SIGN(1)>30.)",
    #### Combination- and mother cuts (b hadrons)
    "BcombCut"        : "(ASUM(SUMTREE(PT,(ISBASIC | (ID=='gamma')),0.))>4600*MeV) & in_range(5100,AM,5600) & (ADOCACHI2CUT(24,''))",
    "BscombCut"       : "(ASUM(SUMTREE(PT,(ISBASIC | (ID=='gamma')),0.))>4700*MeV) & in_range(5300,AM,5600) & (ADOCACHI2CUT(24,''))",
    "LbcombCut"       : "(ASUM(SUMTREE(PT,(ISBASIC | (ID=='gamma')),0.))>5000*MeV) & in_range(5000,AM,6500) & (ADOCACHI2CUT(24,''))",
    "XibcombCut"      : "(ASUM(SUMTREE(PT,(ISBASIC | (ID=='gamma')),0.))>5180*MeV) & in_range(5180,AM,6680) & (ADOCACHI2CUT(24,''))",
    "OmegabcombCut"   : "(ASUM(SUMTREE(PT,(ISBASIC | (ID=='gamma')),0.))>5400*MeV) & in_range(5400,AM,7100) & (ADOCACHI2CUT(24,''))",
    "BccombCut"       : "(ASUM(SUMTREE(PT,(ISBASIC | (ID=='gamma')),0.))>5600*MeV) & ((in_range(5120,AM,5380)) | (in_range(6120,AM,6380))) & (ADOCACHI2CUT(24,''))",
    "XbKSCut"         : "(VFASPF(VCHI2PDOF)<8) & (BPVIPCHI2()<14) & (BPVDIRA>0.9997) & (D2DVVDCHI2SIGN(3)>20.)",
    "Xb4ProngCut"     : "(VFASPF(VCHI2PDOF)<8) & (BPVIPCHI2()<18) & (BPVDIRA>0.9990)",
    "Xb5ProngCut"     : "(VFASPF(VCHI2PDOF)<8) & (BPVIPCHI2()<14) & (BPVDIRA>0.9997)",
    "Xb6ProngCut"     : "(VFASPF(VCHI2PDOF)<8) & (BPVIPCHI2()<10) & (BPVDIRA>0.9998)",
    "XbSt2XbhMCut"    : "(VFASPF(VCHI2PDOF)<8)",
    "XbSt2XbhCCut"    : "(ADOCACHI2CUT(16, '') ) & ((AM - AM1)<( 200*MeV )) & (in_range(5745, AM1, 5845))",
    # for decays with hyperons
    "BCut"            : "(VFASPF(VCHI2PDOF)<8) & (BPVIPCHI2()<18) & (BPVDIRA>0.9990) & (D2DVVDCHI2SIGN(2)>30.) & in_range(5150,M,5550)",
    "BsCut"           : "(VFASPF(VCHI2PDOF)<8) & (BPVIPCHI2()<18) & (BPVDIRA>0.9990) & (D2DVVDCHI2SIGN(2)>30.) & (D2DVVDCHI2SIGN(3)>30.) & in_range(5320,M,5550)",
    "XbCut"           : "(VFASPF(VCHI2PDOF)<8) & (BPVIPCHI2()<18) & (BPVDIRA>0.9990) & (D2DVVDCHI2SIGN(2)>30.)",
    "XbhCut"          : "(VFASPF(VCHI2PDOF)<8) & (BPVIPCHI2()<14) & (BPVDIRA>0.9997) & (D2DVVDCHI2SIGN(2)>40.)",
    "XbSCut"          : "(VFASPF(VCHI2PDOF)<8) & (BPVIPCHI2()<14) & (BPVDIRA>0.9997) & (D2DVVDCHI2SIGN(2)>20.)",
    "XbhhCut"         : "(VFASPF(VCHI2PDOF)<8) & (BPVIPCHI2()<10) & (BPVDIRA>0.9998) & (D2DVVDCHI2SIGN(2)>50.)",
    "XbLKSCut"        : "(VFASPF(VCHI2PDOF)<8) & (BPVIPCHI2()<14) & (BPVDIRA>0.9997) & (D2DVVDCHI2SIGN(2)>40.) & (D2DVVDCHI2SIGN(3)>20.)",
  }

  def selection ( self, cfg ):
    # define here the selection strategy

    #### Skip event if stripping decision negative ####
    fltrs = LoKi_Filters ( STRIP_Code = " HLT_PASS_RE('StrippingFullDSTDiMuonJpsi2MuMuDetachedLineDecision') " )
    DaVinci().EventPreFilters = [fltrs.sequence('LoKiStrippingFilter'+self.name())]

    #### Converts TES locations into AutomaticData
    self.psi  = AutomaticData( cfg["JpsiTES"] )
    self.pi   = AutomaticData( cfg["piTES"] )
    self.K    = AutomaticData( cfg["KTES"] )
    self.p    = AutomaticData( cfg["pTES"] )
    self.piD  = AutomaticData( cfg["piDTES"] )
    self.KD   = AutomaticData( cfg["KDTES"] )
    self.pD   = AutomaticData( cfg["pDTES"] )
    self.phi  = AutomaticData( cfg["phiTES"] )
    self.LDD  = AutomaticData( cfg["LambdaDDTES"] )
    self.LLL  = AutomaticData( cfg["LambdaLLTES"] )
    self.pizm = AutomaticData( cfg["pi0MerTES"] )
    self.pizr = AutomaticData( cfg["pi0ResTES"] )
    self.pizd = AutomaticData( cfg["pi0DalTES"] )
    self.KSDD = AutomaticData( cfg["KSDDTES"] )
    #self.KSLL = AutomaticData( cfg["KSLLTES"] )

    ####
    # Filter and merge tracks
    ####
    #### Filter long tracks
    pi = FilterSelection( self.name() + "DetachedLongPions",   [ self.pi ], Code = cfg["hLKinCut"] )
    K  = FilterSelection( self.name() + "DetachedLongKaons",   [ self.K ],  Code = cfg["hLKinCut"] )
    p  = FilterSelection( self.name() + "DetachedLongProtons", [ self.p ],  Code = cfg["hLKinCut"] )

    ANNpi = FilterSelection( self.name() + "DetachedLongANNPions",   [ pi ], Code = "(PROBNNpi>{ANNCut})".format(**cfg))
    ANNK  = FilterSelection( self.name() + "DetachedLongANNKaons",   [ K ],  Code = "(PROBNNk >{ANNCut})".format(**cfg))
    ANNp  = FilterSelection( self.name() + "DetachedLongANNProtons", [ p ],  Code = "(PROBNNp >{ANNCut})".format(**cfg))

    #### Filter downstream tracks
    piD = FilterSelection( self.name() + "DetachedDownPions",   [ self.piD ], Code = cfg["hDKinCut"] )
    KD  = FilterSelection( self.name() + "DetachedDownKaons",   [ self.KD ],  Code = cfg["hDKinCut"] )
    pD  = FilterSelection( self.name() + "DetachedDownProtons", [ self.pD ],  Code = cfg["hDKinCut"] )

    #### Merge long and downstream tracks
    piLD = MergedSelection( self.name() + "MergedLDPions",   RequiredSelections = [ANNpi,piD])
    KLD  = MergedSelection( self.name() + "MergedLDKaons",   RequiredSelections = [ANNK, KD])
    pLD  = MergedSelection( self.name() + "MergedLDProtons", RequiredSelections = [ANNp, pD])

    ####
    # Build, filter and merge intermediate particles
    ####
    #### Filter pi0s
    pizm = FilterSelection( self.name() + "MergedPi0s",   [ self.pizm ], Code = "(PT>{pi0pT}) & (CL>{pi0CL})".format(**cfg) )
    pizr = FilterSelection( self.name() + "ResolvedPi0s", [ self.pizr ], Code = "(PT>{pi0pT}) & (CL>{pi0CL})".format(**cfg) )
    pizd = FilterSelection( self.name() + "DalitzPi0s",   [ self.pizd ], Code = "(PT>{pi0pT})".format(**cfg) )

    #### Filter the Jpsi
    Jpsi = FilterSelection (cfg["JpsiPreselName"], [ self.psi ], Code = cfg["JpsiMassRange"])

    #### Build a KS -> pi pi from long tracks
    KSLL = SimpleSelection (cfg["KSLLPreselName"], CombineParticles, [ ANNpi ] ,
                            DecayDescriptors = [cfg["KSLLdecay"]] , CombinationCut = cfg["KSLLcombCut"], MotherCut = cfg["KSLLCut"] )

    ### Merge tracks and intermediate particles
    piM    = MergedSelection(self.name() + "MergedLUPions", RequiredSelections = [ANNpi,AutomaticData(cfg["piUTES"])])
    Lambda = MergedSelection(self.name() + "MergedLambdas", RequiredSelections = [self.LDD,self.LLL])
    piz    = MergedSelection(self.name() + "Mergedpi0s", RequiredSelections = [pizm,pizr,pizd])
    KShort = MergedSelection(self.name() + "MergedKShorts", RequiredSelections = [self.KSDD,KSLL])

    #### Filter the phi
    phi = FilterSelection (cfg["phiPreselName"], [self.phi], Code = "CHILDCUT(1, {hLKinCut}) & CHILDCUT(2, {hLKinCut})".format ( **cfg ))

    #### Build a Sigma+ -> p pi0
    Sigma = SimpleSelection (cfg["SigmaPreselName"], CombineParticles, [ pLD, piz ] ,
                             DecayDescriptors = [cfg["Sigmadecay"]] , CombinationCut = cfg["SigmacombCut"], MotherCut = cfg["SigmaCut"] )

    #### Build a Xi- -> Lambda Pi- (pi down Lambda LL combination should be excluded by D2DVVDCHI2SIGN cut)
    Xi = SimpleSelection (cfg["XiPreselName"], CombineParticles, [ Lambda, piLD ] ,
                          DecayDescriptors = [cfg["Xidecay"]] , CombinationCut = cfg["XicombCut"], MotherCut = cfg["XiCut"] )

    #### Build a Omega- -> Lambda K- (K down Lambda LL combination should be excluded by D2DVVDCHI2SIGN cut)
    Omega = SimpleSelection (cfg["OmegaPreselName"], CombineParticles, [ Lambda, KLD ] ,
                             DecayDescriptors = [cfg["Omegadecay"]] , CombinationCut = cfg["OmegacombCut"], MotherCut = cfg["OmegaCut"] )

    ####
    # Build b-hadrons without strange baryons
    ####
    #### B -> Jpsi p pbar
    Jppsel = SimpleSelection ("JpsiPPbar", CombineParticles, [ Jpsi, ANNp ], DecayDescriptors = [cfg["Jppdecay"]],
                              CombinationCut = cfg["BcombCut"], MotherCut = cfg["Xb4ProngCut"] )
    #### Lb -> Jpsi p pi
    Jppisel = SimpleSelection ("JpsiPPi", CombineParticles, [ Jpsi, ANNp, ANNpi ], DecayDescriptors = [cfg["Jppidecay"]],
                               CombinationCut = cfg["LbcombCut"], MotherCut = cfg["Xb4ProngCut"] )
    #### Lb -> Jpsi p K
    JpKsel = SimpleSelection ("JpsiPK", CombineParticles, [ Jpsi, p, K ], DecayDescriptors = [cfg["JpKdecay"]],
                              CombinationCut = cfg["LbcombCut"], MotherCut = cfg["Xb4ProngCut"] )
    #### Lb -> Jpsi p KS pi
    JpKSpisel = SimpleSelection ("JpsiPKSPi", CombineParticles, [ Jpsi, ANNp, KShort, ANNpi ], DecayDescriptors = [cfg["JpKSpidecay"]],
                                 CombinationCut = cfg["LbcombCut"], MotherCut = cfg["XbKSCut"] )
    #### Lb -> Jpsi p pi pi pi
    Jppipipisel = SimpleSelection ("JpsiPPiPiPi", CombineParticles, [ Jpsi, ANNp, ANNpi ] , DecayDescriptors = [cfg["Jppipipidecay"]],
                                   CombinationCut = cfg["LbcombCut"], MotherCut = cfg["Xb6ProngCut"] )
    #### Lb -> Jpsi p K pi pi
    JpKpipisel = SimpleSelection ("JpsiPKPiPi", CombineParticles, [ Jpsi, ANNp, ANNK, ANNpi ], DecayDescriptors = [cfg["JpKpipidecay"]],
                                  CombinationCut = cfg["LbcombCut"], MotherCut = cfg["Xb6ProngCut"] )
    #### Lb -> Jpsi p K K K
    JpKKKsel = SimpleSelection ("JpsiPKKK", CombineParticles, [ Jpsi, ANNp, ANNK ], DecayDescriptors = [cfg["JpKKKdecay"]],
                                CombinationCut = cfg["LbcombCut"], MotherCut = cfg["Xb6ProngCut"] )
    #### Xib -> Jpsi p K K
    JpKKsel = SimpleSelection ("JpsiPKK", CombineParticles, [ Jpsi, p, K ], DecayDescriptors = [cfg["JpKKdecay"]],
                               CombinationCut = cfg["XibcombCut"], MotherCut = cfg["Xb5ProngCut"] )
    #### Xib -> Jpsi p K pi
    JpKpisel = SimpleSelection ("JpsiPKPi", CombineParticles, [ Jpsi, ANNp, ANNK, ANNpi ], DecayDescriptors = [cfg["JpKpidecay"]],
                                CombinationCut = cfg["XibcombCut"], MotherCut = cfg["Xb5ProngCut"] )
    #### Xib -> Jpsi p KS K
    JpKSKsel = SimpleSelection ("JpsiPKSK", CombineParticles, [ Jpsi, ANNp, ANNK, KShort ], DecayDescriptors = [cfg["JpKSKdecay"]],
                                CombinationCut = cfg["XibcombCut"], MotherCut = cfg["XbKSCut"] )
    #### Xib -> Jpsi p K K pi
    JpKKpisel = SimpleSelection ("JpsiPKKPi", CombineParticles, [ Jpsi, ANNp, ANNK, ANNpi ], DecayDescriptors = [cfg["JpKpiKdecay"]],
                                 CombinationCut = cfg["XibcombCut"], MotherCut = cfg["Xb6ProngCut"] )
    #### Xib* -> Xib pi
    Xibpisel = SimpleSelection ("XibPi", CombineParticles, [ JpKKsel, piM ], DecayDescriptors = [cfg["Xibpidecay"]],
                                CombinationCut = cfg["XbSt2XbhCCut"], MotherCut = cfg["XbSt2XbhMCut"] )
    #### Bc -> Jpsi p pbar pi
    Jpppisel = SimpleSelection ("JpsiPPbarPi", CombineParticles, [ Jpsi, ANNp, ANNpi ], DecayDescriptors = [cfg["Jpppidecay"]],
                                CombinationCut = cfg["BcombCut"], MotherCut = cfg["Xb5ProngCut"] )
    ####
    # Build b-hadrons with hyperons
    ####
    #### B -> Jpsi Lambda pbar
    JLpsel = SimpleSelection ("JpsiLambdaPbar", CombineParticles, [ Jpsi, Lambda, ANNp ], DecayDescriptors = [cfg["JLpdecay"]] ,
                              CombinationCut = cfg["BcombCut"], MotherCut = cfg["BCut"] )
    #### Bs -> Jpsi Lambda pbar
    JLLsel = SimpleSelection ("JpsiLambdaLambdabar", CombineParticles, [ Jpsi, Lambda ], DecayDescriptors = [cfg["JLLdecay"]],
                              CombinationCut = cfg["BscombCut"], MotherCut = cfg["BsCut"] )
    #### Lb -> Jpsi Lambda
    JLsel = SimpleSelection ("JpsiLambda", CombineParticles, [ Jpsi, Lambda ], DecayDescriptors = [cfg["JLdecay"]],
                             CombinationCut = cfg["LbcombCut"], MotherCut = cfg["XbCut"] )
    #### Lb -> Jpsi Lambda pi pi
    JLPiPisel = SimpleSelection ("JpsiLambdaPiPi", CombineParticles, [ Jpsi, Lambda, ANNpi ], DecayDescriptors = [cfg["JLPiPidecay"]],
                                 CombinationCut = cfg["LbcombCut"], MotherCut = cfg["XbhhCut"] )
    #### Lb -> Jpsi Lambda phi
    JLPhisel = SimpleSelection ("JpsiLambdaPhi", CombineParticles, [ Jpsi, Lambda, phi ], DecayDescriptors = [cfg["JLPhidecay"]],
                                CombinationCut = cfg["LbcombCut"], MotherCut = cfg["XbCut"] )
    #### Lb -> Jpsi Sigma pi
    JSPisel = SimpleSelection ("JpsiSigmaPi", CombineParticles, [ Jpsi, Sigma, ANNpi ], DecayDescriptors = [cfg["JSPidecay"]],
                               CombinationCut = cfg["LbcombCut"], MotherCut = cfg["XbSCut"] )
    #### Lb -> Jpsi Xi K
    JXiKsel = SimpleSelection ("JpsiXiK", CombineParticles, [ Jpsi, Xi, ANNK ], DecayDescriptors = [cfg["JXiKdecay"]],
                               CombinationCut = cfg["LbcombCut"], MotherCut = cfg["XbhCut"] )
    #### Xib -> Jpsi Lambda K
    JLKsel = SimpleSelection ("JpsiLambdaK", CombineParticles, [ Jpsi, Lambda, ANNK ], DecayDescriptors = [cfg["JLKdecay"]],
                              CombinationCut = cfg["XibcombCut"], MotherCut = cfg["XbhCut"] )
    #### Xib -> Jpsi Lambda K pi
    JLKPisel = SimpleSelection ("JpsiLambdaKPi", CombineParticles, [ Jpsi, Lambda, ANNK, ANNpi ], DecayDescriptors = [cfg["JLKPidecay"]],
                                CombinationCut = cfg["XibcombCut"], MotherCut = cfg["XbhhCut"] )
    #### Xib -> Jpsi Lambda KS
    JLKSsel = SimpleSelection ("JpsiLambdaKS", CombineParticles, [ Jpsi, Lambda, KShort ], DecayDescriptors = [cfg["JLKSdecay"]],
                               CombinationCut = cfg["XibcombCut"], MotherCut = cfg["XbLKSCut"] )
    #### Xib -> Jpsi Xi
    JXisel = SimpleSelection ("JpsiXi", CombineParticles, [ Jpsi, Xi ], DecayDescriptors = [cfg["JXidecay"]],
                              CombinationCut = cfg["XibcombCut"], MotherCut = cfg["XbCut"] )
    #### Xib -> Jpsi Xi pi
    JXiPisel = SimpleSelection ("JpsiXiPi", CombineParticles, [ Jpsi, Xi, ANNpi ], DecayDescriptors = [cfg["JXiPidecay"]],
                                CombinationCut = cfg["XibcombCut"], MotherCut = cfg["XbhCut"] )
    #### Xib -> Jpsi Xi pi pi
    JXiPiPisel = SimpleSelection ("JpsiXiPiPi", CombineParticles, [ Jpsi, Xi, ANNpi ], DecayDescriptors = [cfg["JXiPiPidecay"]],
                                  CombinationCut = cfg["XibcombCut"], MotherCut = cfg["XbhhCut"] )
    #### Omegab -> Jpsi Omega
    JOmegasel = SimpleSelection ("JpsiOmega", CombineParticles, [ Jpsi, Omega ] , DecayDescriptors = [cfg["JOmegadecay"]],
                                 CombinationCut = cfg["OmegabcombCut"], MotherCut = cfg["XbCut"] )
    #### Omegab -> Jpsi Omega pi pi
    JOmegaPiPisel = SimpleSelection ("JpsiOmegaPiPi", CombineParticles, [ Jpsi, Omega, ANNpi ], DecayDescriptors = [cfg["JOmegaPiPidecay"]],
                                     CombinationCut = cfg["OmegabcombCut"], MotherCut = cfg["XbhhCut"] )

    return [Jppsel, Jppisel, JpKsel, JpKSpisel, Jppipipisel, JpKpipisel, JpKKKsel, JpKKsel, JpKpisel, JpKSKsel, JpKKpisel, Xibpisel, Jpppisel,
            JLpsel, JLLsel, JLsel, JLPiPisel, JLPhisel, JSPisel, JXiKsel, JLKsel, JLKPisel, JLKSsel, JXisel, JXiPisel, JXiPiPisel, JOmegasel, JOmegaPiPisel]

################################################################################
## Interface for automatic testing
################################################################################

if __name__ == '__builtin__':
  # if the file is included as an option, it includes the line.
  # while waits for configuration if it is import-ed
  JpsiBaryonX("WGProdLineJpsiBaryonX")
  
  f = file('list_of_lines.log', 'w')
  # name of the selection returned from selection() function
  f.write ( 'JpsiBaryonXLine' )
  f.close()
