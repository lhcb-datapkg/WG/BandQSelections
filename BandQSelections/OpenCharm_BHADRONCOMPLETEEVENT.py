#!/usr/bin/env gaudirun.py
# -*- coding: utf-8 -*-
# =============================================================================
## @file WGProdLine.py
#  Base class for WGProduction lines
#  @attention DataType and CondDB are not specified!
#  Usage: override and implement selection()
#  
#  @author Marian Stahl    marian.stahl@cern.ch
#  @author Lucio Anderlini lucio.anderlini@cern.ch
#  @date   2018-12-13
# =============================================================================
""" 
Open charm selections from BHADRONCOMPLETEEVENT.MDST

"""

from WGProdTools import WGProdLine

################################################################################
from PhysSelPython.Wrappers import AutomaticData
from PhysConf.Selections import SimpleSelection, FilterSelection
from GaudiConfUtils.ConfigurableGenerators import CombineParticles
from Configurables import DaVinci
from PhysConf.Filters import LoKi_Filters

class OpenCharm_BHADRONCOMPLETEEVENT ( WGProdLine ):
  defaults = {
    #### Input TES locations ####
    "piTES"          : 'Phys/StdLooseANNPions/Particles', # IPCHI2>4, PT>250 and ProbNNpi>0.05
    "DDsTES"         : 'B02DDBeauty2CharmLine', # only full DST in Run2
    "DDTES"          : 'B02DDBeauty2CharmLine', # only full DST in Run2
    "DPiTES"         : 'B02DPiD2HHHBeauty2CharmLine',
    "D0PiTES"        : 'B2D0PiD2HHBeauty2CharmLine',
    "D0PiD0K3PiTES"  : 'B2D0PiD2HHHHTIGHTBeauty2CharmLine',
    "LcPiTES"        : 'Lb2LcPiNoIPLc2PKPiBeauty2CharmLine',
    "Xic0PiTES"      : 'Xib2Xic0PiNoIPXic02PKKPiBeauty2CharmLine',
    "XicPiTES"       : 'Lb2XicPiNoIPXic2PKPiBeauty2CharmLine',
    "OmegacPiTES"    : 'Omegab2Omegac0PiNoIPOmegac02PKKPiBeauty2CharmLine',
    "Xic0PiPiTES"    : 'Xic02PKKPiBeauty2Charm',
    "XicPiPiSSTES"   : 'Xic2PKPiBeauty2Charm',
    "XicPiPiOSTES"   : 'Xic2PKPiBeauty2Charm',
    "Xic04PiTES"     : 'Xic02PKKPiBeauty2Charm',
    "Xic4PiSSTES"    : 'Xic2PKPiBeauty2Charm',
    "Xic4PiOSTES"    : 'Xic2PKPiBeauty2Charm',
    "OmegacPiPiTES"  : 'Omegac02PKKPiBeauty2Charm',
    "Omegac3PiTES"   : 'Omegac02PKKPiBeauty2Charm',

    #### Decay descriptor for filtering (no CC for some to be compatible to the ones in the stripping) ####
    "DDsdec"         : "(B0 -> (D- -> pi- pi- K+) (D+ -> K+ pi+ K-)) || (B0 -> (D+ -> pi+ pi+ K-) (D- -> K- pi- K+))",
    "DDdec"          : "(B0 -> (D- -> pi- pi- K+) (D+ -> pi+ pi+ K-)) || (B0 -> (D+ -> pi+ pi+ K-) (D- -> pi- pi- K+))",
    "DPidec"         : "[B~0 -> (D+ -> pi+ pi+ K-) pi-]CC",
    "D0Pidec"        : "(B- -> (D0 -> K- pi+) pi-) || (B+ -> (D0 -> K+ pi-) pi+)",
    "D0PiD0K3Pidec"  : "(B- -> (D0 -> pi+ pi+ K- pi-) pi-) || (B+ -> (D0 -> K+ pi+ pi- pi-) pi+)",
    "LcPidec"        : "[Lambda_b0 -> (Lambda_c+ -> p+ K- pi+) pi-]CC",
    "Xic0Pidec"      : "[Xi_b- -> (Xi_c0 -> p+ K- K- pi+) pi-]CC",
    "XicPidec"       : "[Lambda_b0 -> (Xi_c+ -> p+ K- pi+) pi-]CC",
    "OmegacPidec"    : "[Omega_b- -> (Omega_c0 -> p+ K- K- pi+) pi-]CC",
    "Xic0PiPidec"    : "[Xi_b0 -> Xi_c0 pi- pi+]cc",
    "XicPiPiSSdec"   : "[Xi_b- -> Xi_c+ pi- pi-]cc",
    "XicPiPiOSdec"   : "[Xi_bc+ -> Xi_c+ pi- pi+]cc",
    "Xic04Pidec"     : "[Xi_b0 -> Xi_c0 pi- pi+ pi- pi+]cc",
    "Xic4PiSSdec"    : "[Xi_b- -> Xi_c+ pi- pi- pi- pi+]cc",
    "Xic4PiOSdec"    : "[Xi_bc+ -> Xi_c+ pi- pi+ pi- pi+]cc",
    "OmegacPiPidec"  : "[Omega_bc0 -> Omega_c0 pi+ pi-]cc",
    "Omegac3Pidec"   : "[Omega_b- -> Omega_c0 pi- pi+ pi-]cc",

    #### Cuts ####
    "DDscut"         : "in_range(1820., M1, 1920.) & in_range(1820., M2, 2020.)",
    "DDcut"          : "in_range(1820., M1, 1920.) & in_range(1820., M2, 1920.)",
    "DPicut"         : "in_range(1820., M1, 1920.)",
    "D0Picut"        : "in_range(1815., M1, 1915.)",
    "D0PiD0K3Picut"  : "in_range(1815., M1, 1915.)",
    "LcPicut"        : "in_range(2236., M1, 2336.) & (BPVIPCHI2()<25) & (BPVDIRA>0.999)",
    "Xic0Picut"      : "in_range(2420., M1, 2520.) & (BPVIPCHI2()<25) & (BPVDIRA>0.999)",
    "XicPicut"       : "in_range(2420., M1, 2520.) & (BPVIPCHI2()<25) & (BPVDIRA>0.999)",
    "OmegacPicut"    : "in_range(2645., M1, 2745.) & (BPVIPCHI2()<25) & (BPVDIRA>0.999)",
    "Xic0PiPiCcut"   : "(ASUM(SUMTREE(PT,(ISBASIC | (ID=='gamma')),0.))>5180*MeV) & in_range(2420., AM1, 2520.) & in_range(5200,AM,7500) & (ADOCACHI2CUT(24,''))",
    "XicPiPiSSCcut"  : "(ASUM(SUMTREE(PT,(ISBASIC | (ID=='gamma')),0.))>5180*MeV) & in_range(2420., AM1, 2520.) & in_range(5200,AM,7500) & (ADOCACHI2CUT(24,''))",
    "XicPiPiOSCcut"  : "(ASUM(SUMTREE(PT,(ISBASIC | (ID=='gamma')),0.))>5180*MeV) & in_range(2420., AM1, 2520.) & in_range(5200,AM,7500) & (ADOCACHI2CUT(24,''))",
    "Xic04PiCcut"    : "(ASUM(SUMTREE(PT,(ISBASIC | (ID=='gamma')),0.))>5180*MeV) & in_range(2420., AM1, 2520.) & in_range(5200,AM,7500) & (ADOCACHI2CUT(24,''))",
    "Xic4PiSSCcut"   : "(ASUM(SUMTREE(PT,(ISBASIC | (ID=='gamma')),0.))>5180*MeV) & in_range(2420., AM1, 2520.) & in_range(5200,AM,7500) & (ADOCACHI2CUT(24,''))",
    "Xic4PiOSCcut"   : "(ASUM(SUMTREE(PT,(ISBASIC | (ID=='gamma')),0.))>5180*MeV) & in_range(2420., AM1, 2520.) & in_range(5200,AM,7500) & (ADOCACHI2CUT(24,''))",
    "OmegacPiPiCcut" : "(ASUM(SUMTREE(PT,(ISBASIC | (ID=='gamma')),0.))>5400*MeV) & in_range(2645., AM1, 2745.) & in_range(5500,AM,7700) & (ADOCACHI2CUT(24,''))",
    "Omegac3PiCcut"  : "(ASUM(SUMTREE(PT,(ISBASIC | (ID=='gamma')),0.))>5400*MeV) & in_range(2645., AM1, 2745.) & in_range(5500,AM,6800) & (ADOCACHI2CUT(24,''))",
    "Xic0PiPiMcut"   : "(VFASPF(VCHI2PDOF)<10) & (BPVIPCHI2()<21) & (BPVDIRA>0.999)",
    "XicPiPiSSMcut"  : "(VFASPF(VCHI2PDOF)<10) & (BPVIPCHI2()<21) & (BPVDIRA>0.999)",
    "XicPiPiOSMcut"  : "(VFASPF(VCHI2PDOF)<10) & (BPVIPCHI2()<21) & (BPVDIRA>0.999)",
    "Xic04PiMcut"    : "(VFASPF(VCHI2PDOF)<8) & (BPVIPCHI2()<14) & (BPVDIRA>0.9997)",
    "Xic4PiSSMcut"   : "(VFASPF(VCHI2PDOF)<8) & (BPVIPCHI2()<14) & (BPVDIRA>0.9997)",
    "Xic4PiOSMcut"   : "(VFASPF(VCHI2PDOF)<8) & (BPVIPCHI2()<14) & (BPVDIRA>0.9997)",
    "OmegacPiPiMcut" : "(VFASPF(VCHI2PDOF)<10) & (BPVIPCHI2()<21) & (BPVDIRA>0.999)",
    "Omegac3PiMcut"  : "(VFASPF(VCHI2PDOF)<8) & (BPVIPCHI2()<18) & (BPVDIRA>0.999)",

    #### Define which decays should be processed? ####
    "Decays"         : ["DDs","DD","DPi","D0Pi","D0PiD0K3Pi","LcPi","Xic0Pi","XicPi","OmegacPi"],
    "Combinations"   : ["Xic0PiPi","XicPiPiSS","XicPiPiOS","Xic04Pi","Xic4PiSS","Xic4PiOS","OmegacPiPi","Omegac3Pi"]#
  }

  def selection ( self, cfg ):
    # define here the selection strategy
    # we do the same for every line. that makes it a lot easier
    selections = []
    filterstring = ""

    for dec in cfg["Decays"]:
      linename = cfg[dec+"TES"]
      TES = "/Event/BhadronCompleteEvent/Phys/" + linename + "/Particles"
      #### Make mass cuts and filter by decay desriptor to keep the rate down and only select the charm we want ####
      selections.append(FilterSelection(dec,[AutomaticData(TES)], Code = "{0} & DECTREE('{1}')".format(cfg[dec+"cut"],cfg[dec+"dec"])))
      filterstring += "HLT_PASS('Stripping" + linename + "Decision') | "

    for comb in cfg["Combinations"]:
      linename = cfg[comb+"TES"]
      charm = AutomaticData("/Event/BhadronCompleteEvent/Phys/" + linename + "/Particles")
      #### Build the new decay ####
      selections.append(SimpleSelection (comb, CombineParticles, [ charm, AutomaticData(cfg["piTES"]) ], DecayDescriptors = [cfg[comb+"dec"]],
                                         CombinationCut = cfg[comb+"Ccut"], MotherCut = cfg[comb+"Mcut"] ))
      filterstring += "HLT_PASS('Stripping" + linename + "Decision') | "

    #### Skip event if stripping decision negative ####
    fltrs = LoKi_Filters ( STRIP_Code = filterstring[:-3])
    DaVinci().EventPreFilters = [fltrs.sequence('LoKiStrippingFilter'+self.name())]

    return selections

  ################################################################################
  ## Interface for automatic testing
  ################################################################################

if __name__ == '__builtin__':
    # if the file is included as an option, it includes the line.
    # while waits for configuration if it is import-ed
    OpenCharm_BHADRONCOMPLETEEVENT("WGProdLineOpenCharm_BHADRONCOMPLETEEVENT")

    f = file('list_of_lines.log', 'w')
    # name of the selection returned from selection() function
    f.write ( 'OpenCharm_BHADRONCOMPLETEEVENTLine' )
    f.close()
