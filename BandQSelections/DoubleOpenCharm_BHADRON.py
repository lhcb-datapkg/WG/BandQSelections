#!/usr/bin/env gaudirun.py
# -*- coding: utf-8 -*-
# =============================================================================
## @file WGProdLine.py
#  Base class for WGProduction lines
#  @attention DataType and CondDB are not specified!
#  Usage: override and implement selection()
#  
#  @author Marian Stahl    marian.stahl@cern.ch
#  @author Lucio Anderlini lucio.anderlini@cern.ch
#  @date   2018-11-26
# =============================================================================
""" 
Double open charm selections from BHADRON.MDST

"""

from WGProdTools import WGProdLine

################################################################################
from PhysSelPython.Wrappers import AutomaticData
from PhysConf.Selections import FilterSelection
from GaudiConfUtils.ConfigurableGenerators import CombineParticles 
from Configurables import DaVinci
from PhysConf.Filters import LoKi_Filters

class DoubleOpenCharm_BHADRON ( WGProdLine ):
  defaults = {
    #### Input TES locations ####
    "DDsTES"      : 'B02DDBeauty2CharmLine', # only in Run1
    "DDTES"       : 'B02DDBeauty2CharmLine', # only in Run1
    "D0DKTES"     : 'B02D0DKBeauty2CharmLine',
    "D0DsKTES"    : 'B02D0DKBeauty2CharmLine',
    "LcD0KTES"    : 'X2LcD0KD02KPiBeauty2CharmLine',
    "LcDsTES"     : 'Lb2LcDD2HHHPIDBeauty2CharmLine',
    "LcDTES"      : 'Lb2LcDD2HHHPIDBeauty2CharmLine',
    "LcDKstTES"   : 'Lb2LcDKstBeauty2CharmLine',
    "LcDsKTES"    : 'Xib2LcDsKLc2PKPiDs2KKPiBeauty2CharmLine',
    "XicDTES"     : 'Xib02XicDXic2PKPiBeauty2CharmLine',
    "XicDsTES"    : 'Xib02XicDXic2PKPiBeauty2CharmLine',
    "Xic0DTES"    : 'Xib2Xic0DXic02PKKPiBeauty2CharmLine',
    "Xic0DsTES"   : 'Xib2Xic0DXic02PKKPiBeauty2CharmLine',
    "LcDspbarTES" : 'B2LcDspbarLc2PKPiDs2KKPiBeauty2CharmLine',
    #### Decay descriptor for filtering (no CC for some to be compatible to the ones in the stripping) ####
    "DDsdec"      : "(B0 -> (D- -> pi- pi- K+) (D+ -> K+ pi+ K-)) || (B0 -> (D+ -> pi+ pi+ K-) (D- -> K- pi- K+))",
    "DDdec"       : "(B0 -> (D- -> pi- pi- K+) (D+ -> pi+ pi+ K-)) || (B0 -> (D+ -> pi+ pi+ K-) (D- -> pi- pi- K+))",
    "D0DKdec"     : "(B0 -> (D0 -> K- pi+) (D- -> pi- pi- K+) K+) || (B0 -> (D0 -> K+ pi-) (D+ -> pi+ pi+ K-) K-)",
    "D0DsKdec"    : "(B0 -> (D0 -> K- pi+) (D- -> K- pi- K+) K+) || (B0 -> (D0 -> K+ pi-) (D+ -> K+ pi+ K-) K-)",
    "LcD0Kdec"    : "(B0 -> (Lambda_c+ -> p+ K- pi+) (D0 -> K+ pi-) K-) || (B0 -> (Lambda_c~- -> p~- K+ pi-) (D0 -> K- pi+) K+)",
    "LcDsdec"     : "[Lambda_b0 -> (Lambda_c+ -> p+ K- pi+) (D- -> K- pi- K+)]CC",
    "LcDdec"      : "[Lambda_b0 -> (Lambda_c+ -> p+ K- pi+) (D- -> pi- pi- K+)]CC",
    #it should be (K*(892)~0 -> K- pi+) in the following. It's wrong in the stripping line, but there are no PID or mass cuts which would reject good K*~ events
    #just make sure you swap masses when making the tuple
    "LcDKstdec"   : "[Lambda_b0 -> (Lambda_c+ -> p+ K- pi+) (D- -> pi- pi- K+) (K*(892)0 -> K+ pi-)]CC",
    "LcDsKdec"    : "[Xi_b- -> (Lambda_c+ -> p+ K- pi+) (D- -> K- pi- K+) K-]CC",
    "XicDdec"     : "[Xi_b0 -> (Xi_c+ -> p+ K- pi+) (D- -> pi- pi- K+)]CC",
    "XicDsdec"    : "[Xi_b0 -> (Xi_c+ -> p+ K- pi+) (D- -> K- pi- K+)]CC",
    "Xic0Ddec"    : "[Xi_b- -> (Xi_c0 -> p+ K- K- pi+) (D- -> pi- pi- K+)]CC",
    "Xic0Dsdec"   : "[Xi_b- -> (Xi_c0 -> p+ K- K- pi+) (D- -> K- pi- K+)]CC",
    "LcDspbardec" : "[B- -> (Lambda_c+ -> p+ K- pi+) (D- -> K- pi- K+) p~-]CC",
    #### Cuts ####
    "DDscut"      : "in_range(1820., M1, 1920.) & in_range(1820., M2, 2020.)",
    "DDcut"       : "in_range(1820., M1, 1920.) & in_range(1820., M2, 1920.)",
    "D0DKcut"     : "in_range(1815., M1, 1915.) & in_range(1820., M2, 1920.)",
    "D0DsKcut"    : "in_range(1815., M1, 1915.) & in_range(1820., M2, 2020.)",
    "LcD0Kcut"    : "in_range(2236., M1, 2336.) & in_range(1815., M2, 1915.)",
    "LcDscut"     : "in_range(2236., M1, 2336.) & in_range(1920., M2, 2020.)",
    "LcDcut"      : "in_range(2236., M1, 2336.) & in_range(1820., M2, 1920.)",
    "LcDKstcut"   : "in_range(2236., M1, 2336.) & in_range(1820., M2, 1920.)",
    "LcDsKcut"    : "in_range(2236., M1, 2336.) & in_range(1820., M2, 2020.)",
    "XicDcut"     : "in_range(2420., M1, 2520.) & in_range(1820., M2, 1920.)",
    "XicDscut"    : "in_range(2420., M1, 2520.) & in_range(1820., M2, 2020.)",
    "Xic0Dcut"    : "in_range(2420., M1, 2520.) & in_range(1820., M2, 1920.)",
    "Xic0Dscut"   : "in_range(2420., M1, 2520.) & in_range(1820., M2, 2020.)",
    "LcDspbarcut" : "in_range(2236., M1, 2336.) & in_range(1820., M2, 2020.) & CHILDCUT(3,(PROBNNp>0.05))",
    #### Define which decays should be processed? ####
    "Decays"      : ["DDs","DD","D0DK","D0DsK","LcD0K","LcDs","LcD","LcDKst","LcDsK","XicD","XicDs","Xic0D","Xic0Ds","LcDspbar"]
  }

  def selection ( self, cfg ):
    # define here the selection strategy
    # we do the same for every line. that makes it a lot easier
    selections = []
    filterstring = ""

    for dec in cfg["Decays"]:
      linename = cfg[dec+"TES"]
      TES = "/Event/Bhadron/Phys/" + linename + "/Particles"
      #### Make mass cuts and filter by decay desriptor to keep the rate down and only select the charm we want ####
      selections.append(FilterSelection(dec,[AutomaticData(TES)], Code = "{0} & DECTREE('{1}')".format(cfg[dec+"cut"],cfg[dec+"dec"])))
      filterstring += "HLT_PASS('Stripping" + linename + "Decision') | "

    #### Skip event if stripping decision negative ####
    fltrs = LoKi_Filters ( STRIP_Code = filterstring[:-3])
    DaVinci().EventPreFilters = [fltrs.sequence('LoKiStrippingFilter'+self.name())]

    return selections

################################################################################
## Interface for automatic testing
################################################################################

if __name__ == '__builtin__':
  # if the file is included as an option, it includes the line. 
  # while waits for configuration if it is import-ed 
  DoubleOpenCharm_BHADRON("WGProdLineDoubleOpenCharm_BHADRON")
  
  f = file('list_of_lines.log', 'w')
  # name of the selection returned from selection() function
  f.write ( 'DoubleOpenCharm_BHADRONLine' )
  f.close()

  

