#!/usr/bin/env gaudirun.py
# -*- coding: utf-8 -*-
# =============================================================================
## @file WGProdLine.py
#  Base class for WGProduction lines
#  @attention DataType and CondDB are not specified!
#  Usage: override and implement selection()
#  
#  @author Saverio Mariani saverio.mariani@cern.ch
#  @date   2018-11-23
# =============================================================================
""" 
Selection for B+ -> ppbar K gamma, B0 -> ppbar K gamma pi through intermediate hc/chic state
"""

from WGProdTools import WGProdLine

################################################################################
from PhysConf.Selections import FilterSelection
from PhysSelPython.Wrappers import AutomaticData 
from PhysConf.Selections import RebuildSelection
from PhysConf.Selections import SimpleSelection
from GaudiConfUtils.ConfigurableGenerators import CombineParticles 
from Configurables import SubstitutePID

class B2ppbarKg_pi_ ( WGProdLine ):

    defaults = {
        #final state particles
        'piTES'       : '/Event/Phys/StdLoosePions/Particles',
        'ppbarTES'    : '/Event/CharmCompleteEvent/Phys/Ccbar2PpbarDetachedLine/Particles',
        'gammaTES'    : '/Event/Phys/StdLoosePhotons/Particles', 
        'gammaCnvTES' : '/Event/Phys/StdLooseCnvPhotons/Particles',
        'KTES'        : '/Event/Phys/StdLooseKaons/Particles',

        #J/psi -> ppbar selection thresholds
        'minppbarP' : '10*GeV',
        
        #Chic -> Jpsi gamma selection thresholds
        'Chic_DecayDescriptor' : 'chi_c1(1P) -> J/psi(1S) gamma',
        'Chic_DaughtersCuts'   : { 'gamma' : '(CL > 0.2) & (PT > 400*MeV)' },
        'Chic_CombinationCuts' : '(AALL)',
        'Chic_MotherCuts'      : '(ALL)',

        #Chic -> Jpsi gamma(Cnv) selection thresholds 
        'ChicCnv_DecayDescriptor' : 'chi_c1(1P) -> J/psi(1S) gamma',
        'ChicCnv_DaughtersCuts'   : { 'gamma' : '(CL > 0.2) & (PT > 400*MeV)' },
        'ChicCnv_CombinationCuts' : '(AALL)',
        'ChicCnv_MotherCuts'      : '(ALL)',

        #B0 -> Chic K pi
        'B02ChicKpi_DecayDescriptor' : ' [B0 -> chi_c1(1P) pi+ K- ]cc ',
        'B02ChicKpi_DaughtersCuts'   : {},
        'B02ChicKpi_CombinationCuts' : "(ADAMASS('B0') < 400*MeV)",
        'B02ChicKpi_MotherCuts'      : ' (VFASPF(VCHI2/VDOF)<9) & (DTF_CHI2NDOF(True) < 10) & (PT > 1500) ',

        #B+ -> Chic K+
        'B2ChicK_DecayDescriptor' : ' [B+ -> chi_c1(1P) K+ ]cc ',
        'B2ChicK_DaughtersCuts'   : {},
        'B2ChicK_CombinationCuts' : "(ADAMASS('B+') < 300*MeV)",
        'B2ChicK_MotherCuts'      : ' (VFASPF(VCHI2/VDOF)<9) & (DTF_CHI2NDOF(True) < 10) & (PT > 1500) ',

        #B0 -> Chic K pi (Cnv)
        'B02ChicCnvKpi_DecayDescriptor' : ' [B0 -> chi_c1(1P) K- pi+ ]cc ',
        'B02ChicCnvKpi_DaughtersCuts'   : {},
        'B02ChicCnvKpi_CombinationCuts' : "(ADAMASS('B0') < 400*MeV)",
        'B02ChicCnvKpi_MotherCuts'      : ' (VFASPF(VCHI2/VDOF)<9) & (DTF_CHI2NDOF(True) < 10) & (PT > 1500) ',

        #B+ -> Chic K+ (Cnv)
        'B2ChicCnvK_DecayDescriptor' : ' [B+ -> chi_c1(1P) K+ ]cc ',
        'B2ChicCnvK_DaughtersCuts'   : {},
        'B2ChicCnvK_CombinationCuts' : "(ADAMASS('B+') < 300*MeV)",
        'B2ChicCnvK_MotherCuts'      : ' (VFASPF(VCHI2/VDOF)<9) & (DTF_CHI2NDOF(True) < 10) & (PT > 1500) ',
        
        }

    def selection ( self, cfg ):

        self.pi       = RebuildSelection ( AutomaticData ( cfg[ 'piTES' ] ))
        self.ppbar    = AutomaticData ( cfg[ 'ppbarTES' ] )
        self.gamma    = RebuildSelection ( AutomaticData ( cfg[ 'gammaTES' ]))
        self.gammaCnv = RebuildSelection ( AutomaticData ( cfg[ 'gammaCnvTES' ])) 
        self.K        = RebuildSelection ( AutomaticData ( cfg[ 'KTES' ]))
        
        ppbar = FilterSelection (
            'Jpsi2ppbar_Selection',
            [ self.ppbar ],
            Code = " (CHILDCUT(P> {minppbarP} ,1)) & (CHILDCUT(P>{minppbarP},2)) ".format(**cfg)
            )
        

        ## Chic -> J/psi gamma selection
        Chic = SimpleSelection (
            "Chic_Selection",
            CombineParticles   ,
            [ ppbar, self.gamma ] ,
            DecayDescriptors = [  cfg["Chic_DecayDescriptor"]  ] ,
            DaughtersCuts    = cfg["Chic_DaughtersCuts"],
            CombinationCut   = cfg["Chic_CombinationCuts"],
            MotherCut        = cfg["Chic_MotherCuts"]     ,
            )

        ## ChicCnv -> J/psi gamma selection
        ChicCnv = SimpleSelection (
            "ChicCnv_Selection",
            CombineParticles   ,
            [ ppbar, self.gammaCnv ] ,
            DecayDescriptors = [  cfg["ChicCnv_DecayDescriptor"]  ] ,
            DaughtersCuts    = cfg["ChicCnv_DaughtersCuts"],
            CombinationCut   = cfg["ChicCnv_CombinationCuts"],
            MotherCut        = cfg["ChicCnv_MotherCuts"]     ,
            )


        ## B0 -> (Chic -> J/psi gamma) K+ pi- selection
        B02ChicKpi = SimpleSelection (
            "B02ChicKpi_Selection",
            CombineParticles   ,
            [ Chic, self.K, self.pi ] ,
            DecayDescriptors = [  cfg["B02ChicKpi_DecayDescriptor"]  ] ,
            DaughtersCuts    = cfg["B02ChicKpi_DaughtersCuts"],
            CombinationCut   = cfg["B02ChicKpi_CombinationCuts"],
            MotherCut        = cfg["B02ChicKpi_MotherCuts"],
            )

        ## B+ -> (Chic -> J/psi gamma) K+ selection
        B2ChicK = SimpleSelection (
            "B2ChicK_Selection",
            CombineParticles   ,
            [ Chic, self.K] ,
            DecayDescriptors = [  cfg["B2ChicK_DecayDescriptor"]  ] ,
            DaughtersCuts    = cfg["B2ChicK_DaughtersCuts"],
            CombinationCut   = cfg["B2ChicK_CombinationCuts"],
            MotherCut        = cfg["B2ChicK_MotherCuts"]     ,
            )


        ## B0 -> (ChicCnv -> J/psi gamma) K+ pi- selection
        B02ChicCnvKpi = SimpleSelection (
            "B02ChicCnvKpi_Selection",
            CombineParticles   ,
            [ ChicCnv, self.K, self.pi ] ,
            DecayDescriptors = [  cfg["B02ChicCnvKpi_DecayDescriptor"]  ] ,
            DaughtersCuts    = cfg["B02ChicCnvKpi_DaughtersCuts"],
            CombinationCut   = cfg["B02ChicCnvKpi_CombinationCuts"],
            MotherCut        = cfg["B02ChicCnvKpi_MotherCuts"]     ,
            )


        ## B+ -> (ChicCnv -> J/psi gamma) K+ selection
        B2ChicCnvK = SimpleSelection (
            "B2ChicCnvK_Selection",
            CombineParticles   ,
            [ ChicCnv, self.K ] ,
            DecayDescriptors = [  cfg["B2ChicCnvK_DecayDescriptor"]  ] ,
            DaughtersCuts    = cfg["B2ChicCnvK_DaughtersCuts"],
            CombinationCut   = cfg["B2ChicCnvK_CombinationCuts"],
            MotherCut        = cfg["B2ChicCnvK_MotherCuts"]     ,
            )

        ## B0 -> (hc -> etac gamma) K+ pi- selection
        B02hcKpi = SubstitutePID( 'B02hcKpi',
                                  Code = "DECTREE('[B0 -> (chi_c1(1P) -> J/psi(1S) gamma) K- pi+]CC')",
                                  Substitutions = {
                'B0  -> ^(X ->  X gamma) K- pi+': 'h_c(1P)',
                'B0  ->  (X -> ^X gamma) K- pi+': 'eta_c(1S)',
                'B~0 -> ^(X ->  X gamma) K+ pi-': 'h_c(1P)',
                'B~0 ->  (X -> ^X gamma) K+ pi-': 'eta_c(1S)',
                },
                                  Inputs = [ B02ChicKpi ]
                                  )


        B02hcKpi_sel = FilterSelection (
            "B02hcKpi_Selection",
            B02hcKpi.getSequence() ,
            Code  = "ALL"
        )


        ## B+ -> (hc -> etac gamma) K+ selection
        B2hcK = SubstitutePID( 'B2hcK',
                               Code = "DECTREE('[B+ -> (chi_c1(1P) -> J/psi(1S) gamma) K+]CC')",
                               Substitutions = {
                                   'B+ -> ^(X ->  X gamma) K+': 'h_c(1P)',
                                   'B+  ->  (X -> ^X gamma) K+': 'eta_c(1S)',
                                   'B~- -> ^(X ->  X gamma) K-': 'h_c(1P)',
                                   'B~- ->  (X -> ^X gamma) K-': 'eta_c(1S)',
                               },
                               Inputs = [ B2ChicK ]
        )


        B2hcK_sel = FilterSelection ( 
        "B2hcK_Selection",
        B2hcK.getSequence() ,
        Code  = "ALL"
        )


        ## B0 -> (hc -> etac gamma (Cnv)) K+ pi- selection
        B02hcCnvKpi = SubstitutePID( 'B02hcCnvKpi',
                                     Code = "DECTREE('[B0 -> (chi_c1(1P) -> J/psi(1S) gamma) K- pi+]CC')",
                                     Substitutions = {
                'B0  -> ^(X ->  X gamma) K- pi+': 'h_c(1P)',
                'B0  ->  (X -> ^X gamma) K- pi+': 'eta_c(1S)',
                'B~0 -> ^(X ->  X gamma) K+ pi-': 'h_c(1P)',
                'B~0 ->  (X -> ^X gamma) K+ pi-': 'eta_c(1S)',
                },
                                     Inputs = [ B02ChicCnvKpi ]
                                     )

        B02hcCnvKpi_sel = FilterSelection (
        "B02hcCnvKpi_Selection",
        B02hcCnvKpi.getSequence() ,
        Code  = "ALL"
        )
        

        ## B+ -> (hc -> etac gamma (Cnv)) K+ selection
        B2hcCnvK = SubstitutePID( 'B2hcCnvK',
                                     Code = "DECTREE('[B+ -> (chi_c1(1P) -> J/psi(1S) gamma) K+]CC')",
                                     Substitutions = {
                'B+  -> ^(X ->  X gamma) K+': 'h_c(1P)',
                'B+  ->  (X -> ^X gamma) K+': 'eta_c(1S)',
                'B~- -> ^(X ->  X gamma) K-': 'h_c(1P)',
                'B~- ->  (X -> ^X gamma) K-': 'eta_c(1S)',
                },
                                     Inputs = [ B2ChicCnvK ]
                                     )

        B2hcCnvK_sel = FilterSelection (
        "B2hcCnvK_Selection",
        B2hcCnvK.getSequence() ,
        Code  = "ALL"
        )


        return [B02ChicKpi, B2ChicK, B02ChicCnvKpi, B2ChicCnvK, B02hcKpi_sel, B2hcK_sel, B02hcCnvKpi_sel, B2hcCnvK_sel]





################################################################################
## Interface for automatic testing
################################################################################

if __name__ == '__builtin__':
  # if the file is included as an option, it includes the line. 
  # while waits for configuration if it is import-ed 
  #JpsiPiPi() 
    B2ppbarKg_pi_()

    f = file('list_of_lines.log', 'w')
    # name of the selection returned from selection() function
    f.write ( 'B2ppbarKg_pi_lines' )
    f.close()
    
