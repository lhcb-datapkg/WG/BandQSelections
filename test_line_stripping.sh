#!/bin/bash
# run with lb-run DaVinci/v42r6p1 test_line_stripping.sh 

export PYTHONPATH=./BandQSelections/:$PYTHONPATH
export BANDQCONFIGROOT=../BandQConfig

gaudirun.py \
    $BANDQCONFIGROOT/options/DataType-2016.py          \
    $BANDQCONFIGROOT/options/Stripping28.py            \
    $BANDQCONFIGROOT/options/EvtMax1000.py             \
    $BANDQCONFIGROOT/options/input-stripping           \
    $@

gaudirun.py \
    tests/DaVinci-Stripping.py


ipython \
    tests/testTuples.py


